#pragma once
#include <fstream>
#include <iostream>


namespace trits 
{
	enum value { True, False, Unknown };
	
	class tritset
	{
	private:
			char *mass; //��������� �� ������
			size_t count; //����� trit'��
	public:
		tritset();
		tritset(size_t); //����������� tritset'a
		void resize(size_t); //��������� ������� tritset'a
		void funcout(std::ostream *); //����� tritset'a
		size_t capacity(); //������ tritset;
		class trit
		{
		private:
			class tritset &pointer; //��������� �� tritset
			size_t num; //����� trit'a
		public:
			trit(tritset &, size_t); //����������� trit'a
			trit operator= (value); //������������ trit'� ��������
			trit operator= (trit); // ������������ trit'y trit
			trit operator~ (); //��������� trit'a
			trit operator& (trit); //���������� &
			trit operator|| (trit); //���������� ||
			value operator== (value);
		};
		trit operator[](size_t); //���������� []


		
	};
	
};