#include "gtest/gtest.h"
#include "header.h"
using namespace trits;

class tritsettest : public ::testing::Test {};

/*TEST_F(tritsettest, mytest1)
{
	std::ofstream fout ("out.txt");
	if (!(fout.is_open())) {
		std::cout << "File doesn't open\n"; system("pause");
	}
	tritset a;
	a[1] = True;
//	a.funcout(&fout);
//	fout.close;
	
} */

TEST_F(tritsettest, mytest2)
{
	//������ ������ ��� �������� 1000 ������
	tritset set(1000);
	// length of internal array
	size_t allocLength = set.capacity();
	assert(allocLength >= 1000 * 2 / 8 / sizeof(size_t));
	// 1000*2 - min bits count
	// 1000*2 / 8 - min bytes count
	// 1000*2 / 8 / sizeof(uint) - min uint[] size

	//�� �������� ������� ������
	set[1000000000] = Unknown;
	assert(allocLength == set.capacity());

	// false, but no exception or memory allocation
	if (set[2000000000] == True) {}
	assert(allocLength == set.capacity());

	//��������� ������
	set[1000000000] = True;
	assert(allocLength < set.capacity());

	//no memory operations
	allocLength = set.capacity();
	set[1000000000] = Unknown;
	set[1000000] = False;
	assert(allocLength == set.capacity());

	//������������ ������ �� ���������� �������� ��� 
	//�� �������� ������������ ��� �������� ���������� �������������� �����
	//� ������ ������ ��� ����� 1000�000
	set.resize(0);
	assert(allocLength > set.capacity());

}

int main(int ac, char *av[])
{
	::testing::InitGoogleTest(&ac, av);
	return RUN_ALL_TESTS(); 
using namespace trits;
}