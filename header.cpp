#include "header.h"
using namespace trits;
tritset::trit::trit(tritset &p, size_t number)
	:pointer(p)
{
	num = number;
}

 
tritset::tritset()
{
	mass = nullptr;
	count = 0;
}

tritset::tritset(size_t size)
{
	if (size == 0) mass = new char[0];
	else {
		size_t x = size / 4 + 1;
		mass = new char[x];
	//	mass = new char[3];
		for (size_t i = 0; i <= x; i++)
			mass[i] = 0;
	}
	count = size;
}

void tritset::resize(size_t size)
{
	char* new_data;
	size_t i;
	size_t x = size / 4 + 1;
	new_data = new char [x];
	if (count != 0)
	{
		for (i = 0; i < (count / 4 + 1); i++)
			new_data[i] = mass[i];
	}
	else new_data[0] = 0;
	for (i = (count / 4 + 1); i < x; i++)
		new_data[i] = 0;
	delete[] mass;
	mass = new_data;
	count = size;
//	delete[] new_data; 
}

void tritset::funcout(std::ostream *fout1)
{
	for (size_t i = 0; i < (this->count / 4 + 1); i++)
		*fout1 << this->mass[i];
}

size_t tritset::capacity()
{
	size_t a;
	a = this->count;
	return a;
}

tritset::trit tritset::operator[](size_t number)
{
	trit a(*this, number);
	return a;
}

tritset::trit tritset::trit::operator=(value ans)
{
	size_t pos, one_byte;
	if ((ans != Unknown) & (num <= this->pointer.count))
	{
		if ((num == 0) && (num >= this->pointer.count)) this->pointer.resize(num + 1);
		else if (num > this->pointer.count)
			this->pointer.resize(num);
		pos = ((num % 4) * 2);
		one_byte = num / 4;
		if (ans == True) {
			this->pointer.mass[one_byte] |= 1 << pos;
			pos++;
			this->pointer.mass[one_byte] |= 1 << pos;
		}
		else
			if (ans == Unknown) {
				this->pointer.mass[one_byte] &= ~(1 << pos);
				pos++;
				this->pointer.mass[one_byte] &= ~(1 << pos);
			}
			else if (ans == False) {
				this->pointer.mass[one_byte] |= 1 << pos;
				pos++;
				this->pointer.mass[one_byte] &= ~(1 << pos);
			}
	}
	return *this;
}

tritset::trit tritset::trit::operator=(trit value_trit)
{
	size_t pos, a;
	value one;
	char one_byte;

	pos = ((value_trit.num % 4) * 2);
	a = (value_trit.num / 4);
	one_byte = value_trit.pointer.mass[a];
	if (num <= this->pointer.count) {
		if (((one_byte >> pos) & 1) == 0) one = Unknown;
		else if (((one_byte >> (pos + 1)) & 1) == 0) one = False;
		else one = True;
	}
	else one = Unknown;
	*this = one;

	return *this;
}

tritset::trit tritset::trit::operator~()
{
	size_t pos, a;
	value one;
	char one_byte;

	pos = ((num % 4) * 2);
	a = (num / 4);
	one_byte = this->pointer.mass[a];
	if (num <= this->pointer.count) {
		if (((one_byte >> pos) & 1) == 0) one = Unknown;
		else if (((one_byte >> (pos + 1)) & 1) == 0) one = True;
		else one = False;
	}
	else one = Unknown;
	*this = one;
	return *this;
}

tritset::trit trits::tritset::trit::operator&(trit q)
{
	size_t pos_left, pos_right, a_left, a_right;
	value one;
	char one_byte_left, one_byte_right;

	pos_left = ((this->num % 4) * 2); pos_right = ((q.num % 4) * 2);
	a_left = (this->num / 4); a_right = (q.num / 4);
	one_byte_left = this->pointer.mass[a_left]; one_byte_right = q.pointer.mass[a_right];

	if ((this->num <= this->pointer.count) & (q.num <= q.pointer.count)) {
		if ((((one_byte_left >> (pos_left + 1)) & 1) == 1) & (((one_byte_right >> (pos_right + 1)) & 1) == 1)) one = True;
		else  if (((((one_byte_left >> pos_left) & 1) == 0) & (((one_byte_right >> pos_right) & 1) == 0)) || ((((one_byte_left >> (pos_left + 1)) & 1) == 1) & (((one_byte_right >> pos_right) & 1) == 0)) || ((((one_byte_left >> pos_left) & 1) == 0) & (((one_byte_right >> (pos_right + 1)) & 1) == 1))) one = Unknown;
		else one = False;
	}
	else if ((this->num > this->pointer.mass[a_left]) & (q.num <= q.pointer.count))
		if ((((one_byte_right >> pos_right) & 1) == 1) & (((one_byte_right >> (pos_right + 1)) & 1) == 0)) one = False;
		else one = Unknown;
	else if ((this->num <= this->pointer.mass[a_left]) & (q.num > q.pointer.count))
		if ((((one_byte_left >> pos_left) & 1) == 1) & (((one_byte_left >> (pos_left + 1)) & 1) == 0)) one = False;
		else one = Unknown;

		*this = one;
		return *this;
}

tritset::trit trits::tritset::trit::operator||(trit q)
{
	
	size_t pos_left, pos_right, a_left, a_right;
	value one;
	char one_byte_left, one_byte_right;

	pos_left = ((this->num % 4) * 2); pos_right = ((q.num % 4) * 2);
	a_left = (this->num / 4); a_right = (q.num / 4);
	one_byte_left = this->pointer.mass[a_left]; one_byte_right = q.pointer.mass[a_right];

	if ((this->num <= this->pointer.count) & (q.num <= q.pointer.count)) {
		if ((((one_byte_left >> pos_left) & 1) == 1) & (((one_byte_left >> (pos_left + 1)) & 1) == 0) & (((one_byte_left >> pos_left) & 1) == 1) & (((one_byte_left >> (pos_left + 1)) & 1) == 0)) one = False;
		else  if (((((one_byte_left >> pos_left) & 1) == 1) & (((one_byte_left >> (pos_left + 1)) & 1) == 0) & (((one_byte_right >> pos_right) & 1) == 0)) || ((((one_byte_left >> pos_left) & 1) == 0) & (((one_byte_right >> pos_right) & 1) == 0)) || ((((one_byte_left >> pos_left) & 1) == 0) & (((one_byte_right >> pos_right) & 1) == 1) & (((one_byte_right >> (pos_right + 1)) & 1) == 0))) one = Unknown;
		else one = True;
	}
	else if ((this->num > this->pointer.mass[a_left]) & (q.num <= q.pointer.count))
		if ((((one_byte_right >> pos_right) & 1) == 0) || ((((one_byte_right >> pos_right) & 1) == 1) & (((one_byte_right >> (pos_right + 1)) & 1) == 0))) one = Unknown;
		else one = True;
	else if ((this->num <= this->pointer.mass[a_left]) & (q.num > q.pointer.count))
		if ((((one_byte_left >> pos_left) & 1) == 0) || ((((one_byte_left >> pos_left) & 1) == 1) & (((one_byte_left >> (pos_left + 1)) & 1) == 0))) one = Unknown;
		else one = True;

		*this = one;
		return *this;
}

value trits::tritset::trit::operator==(value newtrit)
{
	value trit_ans;
	size_t pos, a, one_byte;
	if (this->num <= this->pointer.count)
	{
		pos = ((this->num % 4) * 2);
		a = (this->num / 4);
		one_byte = this->pointer.mass[a];
		if ((((one_byte >> pos) & 1) == 1) & (((one_byte >> (pos + 1)) & 1) == 1)) trit_ans = True;
		else if ((((one_byte >> pos) & 1) == 1) & (((one_byte >> (pos + 1)) & 1) == 0)) trit_ans = False;
		else trit_ans = Unknown;	
	}
	else trit_ans = Unknown;
	
	if (newtrit == trit_ans) return True;
	else return False;
}






